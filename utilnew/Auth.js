const crypto = require('crypto-js')

const apiKey = 'sgNieHjacngMK5g7tQMc9P9f7V7iS73CPHrdJluYivn'
const apiSecret = 'i2aFsESlo0mxAI4YDMdp1lC492b8oEGdOgOILDfA4kt'

const authNonce = Date.now() * 1000
const authPayload = 'AUTH' + authNonce
const authSig = crypto
  .HmacSHA384(authPayload, apiSecret)
  .toString(crypto.enc.Hex)
  
const payload = {
  apiKey,
  authSig,
  authNonce,
  authPayload,
  //dms: 4, uncomment to enable dead-man-switch
  event: 'auth',
  filter: [
    'trading', // orders, positions, trades 
    'trading-tBTCUSD', // tBTCUSD orders, positions, trades
    'funding', // offers, credits, loans, funding trades
    'funding-fBTC', // fBTC offers, credits, loans, funding trades
    'wallet',  // wallet
    'wallet-exchange-BTC', // Exchange BTC wallet changes
    'algo',    // algorithmic orders
    'balance', // balance (tradable balance, ...)
    'notify'   // notifications
  ]
}

module.exports = {
  getAuthObj: JSON.stringify(payload)
}
