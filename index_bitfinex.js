const WebSocket = require('ws')
const wss = new WebSocket('wss://api.bitfinex.com/ws/2')
const auth = require('./utilnew/Auth.js')
const beautify = require("json-beautify")

wss.onmessage = (msg) => {
    console.log(msg.data)
    // console.log(beautify(msg.data, null, 2, 100))
}    
wss.onopen = () => {
  // API keys setup here (See "Authenticated Channels")
  // wss.send(auth.getAuthObj)
  const obj = {
        "event": "subscribe",
        "channel": "trades",
        "pair": "BTCUSD",
        filter: [
        "SEQ",
        "ID",
        "TIMESTAMP",
        "PRICE",
        "AMOUNT"    
        ]
            
    }

  wss.send(JSON.stringify(obj))  
}